<?php 

function _s_widgets_init1() {

	// Footer Dave ~ Fourth Item
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Footer D', '_s' ),
			'id'            => 'footer-d',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

}
add_action( 'widgets_init', '_s_widgets_init1', 100 );


<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<?php

	$banner_height = get_option('lg_option_blog_single_banner_height') ? get_option('lg_option_blog_single_banner_height') : '400px';

?>
	<main>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="blog-banner">
				<?php if(has_post_thumbnail()): ?>
					<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" style="height: <?php echo $banner_height; ?>">
				<?php endif; ?>
			</div>
			<?php the_content(); ?>

		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>
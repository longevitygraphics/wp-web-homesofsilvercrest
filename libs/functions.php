<?php

function other_services( $atts ){
	ob_start();
	?>
	    <?php if( have_rows('page_links', 'option') ): ?>
	        <div class="other-services-slider">
	          <?php
	        while ( have_rows('page_links', 'option') ) : the_row();
	            $image = get_sub_field('image');
	            $title = get_sub_field('title');
	            $description = get_sub_field('description');
	            $page_link = get_sub_field('page_link');
	            ?>
	              <div class="px-3">
	                <a href="<?php echo get_permalink($page_link); ?>">
	                  <div class="single-service">
	                  	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
	                  	<div class="title text-center">
	                  		<h3 class="h4 text-white font-weight-bold"><?php echo $title; ?></h3>
	                  	</div>
	                  </div>
	                </a>
	              </div>
	            <?php
	        endwhile;
	        ?>
	        </div>
	        <?php
	    else :
	        // no rows found
	    endif;
	?>

	<script>
		(function($) {
		
	    $(document).ready(function(){

	    	$('.other-services-slider').slick({
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  arrows: true,
			  dots: true,
	          responsive: [
	            {
	              breakpoint: 1024,
	              settings: {
	                slidesToShow: 3,
	                slidesToScroll: 1,
	                infinite: true,
	                dots: true
	              }
	            },
	            {
	              breakpoint: 900,
	              settings: {
	                slidesToShow: 2,
	                slidesToScroll: 1
	              }
	            },
	            {
	              breakpoint: 550,
	              settings: {
	                slidesToShow: 1,
	                slidesToScroll: 1
	              }
	            }
	            // You can unslick at a given breakpoint now by adding:
	            // settings: "unslick"
	            // instead of a settings object
	          ]
			});
	        
	    });

	}(jQuery));
	</script>
	<?php
	return ob_get_clean();
}
add_shortcode( 'other_services', 'other_services' );

?>
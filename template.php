<?php /* Template Name: Page Template */ ?>

<?php get_header(); ?>

	<div id="site-content" role="main">
		<main>

			<?php flexible_layout(); ?>

		</main>
	</div>

<?php get_footer(); ?>
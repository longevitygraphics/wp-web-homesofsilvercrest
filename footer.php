<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

	<footer id="site-footer">
		
		<div id="site-footer-main" class="clearfix py-3 container-fluid justify-content-center align-items-start flex-wrap">
			<div class="row">
				<div class="site-footer-alpha py-3 col-sm-6 col-md-3 text-center text-md-left"><?php dynamic_sidebar('footer-alpha'); ?></div>
				<div class="site-footer-bravo py-3 col-sm-6 col-md-3 text-center text-md-left"><?php dynamic_sidebar('footer-bravo'); ?></div>
				<div class="site-footer-charlie py-3 col-sm-6 col-md-3 text-center text-md-left"><?php dynamic_sidebar('footer-charlie'); ?></div>
				<div class="site-footer-d py-3 col-sm-6 col-md-3 text-center text-md-left"><?php dynamic_sidebar('footer-d'); ?></div>
			</div>
		</div>

		<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
			<div id="site-legal" class="py-3 px-3 d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		<?php endif; ?>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>
<script src="https://cdn.jsdelivr.net/npm/jquery-selectric@0.1.3/src/jquery.selectric.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/selectric@1.13.0/public/selectric.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.min.css">
</body>
</html>

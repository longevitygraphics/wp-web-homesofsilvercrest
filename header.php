<?php
/**
 * The header for our theme
 *
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> <?php do_action('html_class'); ?>>
<head>
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php do_action('wp_header'); ?>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php do_action('wp_body_start'); ?>

  <div id="page" class="site">

    <div class="header-section">
        <div class="header-popup pt-2 pb-5">
          <?php
            if( have_rows('page_links', 'option') ):
              $other_services = array();
              ?>
                <div class="container-fluid">
                  <div class="row">
                  <?php
                while ( have_rows('page_links', 'option') ) : the_row();
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $page_link = get_sub_field('page_link');
                    $featured_as_main_service = get_sub_field('featured_as_main_service');
                    ?>
                      <?php if($featured_as_main_service == 1): ?>
                        <div class="col-md-6 mb-4">
                          <a href="<?php echo get_permalink($page_link); ?>">
                            <div class="row no-gutters bg-gray-light">
                              <div class="col-sm-4">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                              </div>
                              <div class="col-sm-8 p-3">
                                <h3 class="h4 text-dark font-weight-bold"><?php echo $title; ?></h3>
                                <p class="text-gray"><?php echo $description; ?></p>
                              </div>
                            </div>
                          </a>
                        </div>
                      <?php else: ?>
                      <?php
                        array_push($other_services, array('title'=>$title, 'page_link'=>$page_link));
                      ?>
                      <?php endif; ?>
                    <?php
                endwhile;
                ?>
                    </div>
                </div>

                <?php if($other_services && is_array($other_services)): ?>
                    <div class="pt-4 text-center">
                      <div class="font-weight-bold h4 text-center text-dark mb-3">OTHER SERVICES: </div>
                      <ul class="other-services">
                      <?php foreach ($other_services as $key => $value): ?>
                        <li><a href="<?php echo get_permalink($value['page_link']); ?>"><?php echo $value['title']; ?></a></li>
                      <?php endforeach; ?>
                      </ul>
                    </div>
                <?php endif; ?>
                <?php
            else :
                // no rows found
            endif;
            ?>
        </div>

      <?php
        $banner = get_field('banner');
        $banner_text = get_field('banner_text');
        $banner_button = get_field('banner_button');
      ?>
      <div class="top-banner">
        <?php if($post->post_type == 'page'): ?>
          <img class="img-full" src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>">
        <?php endif; ?>
        <div class="overlay">
          <div class="text"><?php echo $banner_text; ?></div>
          <?php if($banner_button): ?>
            <a class="btn btn-primary mt-4" href="<?php echo $banner_button['url']; ?>"><?php echo $banner_button['title']; ?></a>
          <?php endif; ?>
        </div>

          <header id="site-header" <?php if($post->post_type != 'page'): ?>style="position: relative !important;"<?php endif; ?>>

            <?php do_action('wp_utility_bar'); ?>

            <div class="header-main">
              <div class="d-flex justify-content-between align-items-center flex-wrap px-3 py-4 py-md-2">
                <div class="site-branding">
                  <div class="logo">
                    <a href="<?php echo get_site_url(); ?>"><?php echo site_logo(); ?></a>
                  </div>
                  <div class="mobile-toggle d-md-none"><i class="fa fa-bars" aria-hidden="true"></i></div>
                </div><!-- .site-branding -->

                <div class="align-items-center d-none d-md-flex">
                  <span class="text-uppercase text-white">We build places that inspire.</span>
                  <a class="nav ml-4"><i class="fas fa-bars text-white"></i></a>
                </div>
              </div>
            </div>

          </header><!-- #masthead -->
      </div>
    </div>

    <?php if($post->post_type == 'page'): ?>
    <div class="top-contact container-fluid">
        <div class="row section">
          <div class="col-sm-6">
            <div>
              <i class="fas fa-phone"></i>
            </div>
            <div>
              <div class="text-white h4">CALL</div>
              <a class="text-white" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a>
            </div>
          </div>
          <div class="col-sm-6">
            <div>
              <i class="fas fa-envelope"></i>
            </div>
            <div>
              <div class="text-white h4">EMAIL</div>
              <a class="text-white" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a>
            </div>
          </div>
        </div>
    </div>
  <?php endif; ?>

